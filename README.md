# CarritoFrontend

Este proyecto fue generado con [Angular CLI](https://github.com/angular/angular-cli) version 8.0.0.

## Development server

Ejecutar `ng serve` y navegar a `http://localhost:4200/`.


## Build

Ejecutar `ng build` para compilar el proyecto. El build se almacenará en la carpeta `dist/` del proyecto. Usar el comando  `--prod` para generar una compilación en producción. Para realizar una compilación que funcione en la carpeta dist del servidor (no en la carpeta root) se debe añadir el comando `--base-href='/dist/'`


## Enunciados del Challenge
`http://sd-1357948-h00068.ferozo.net/frontend.pdf`
