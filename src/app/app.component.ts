import { Component, Input } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ProductsService } from 'src/services/products.service';
import Swal from 'sweetalert2';
import { Output } from '@angular/core';
import { result } from 'lodash';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'carrito-frontend';
  @Output() dataSource = new MatTableDataSource <any>([]);
  @Output() dataSourceCarrito = new MatTableDataSource <any>([]);
  @Output() listado = true;
  @Input() listadoChanged;
  @Output() cantidad;
data:any;
  displayedColumns: string[] = ['planes', 'periodos'];
  displayedColumnsCarrito: string[] = ['nombre', 'periodo', 'valor', 'opciones'];

  constructor(private _productsService: ProductsService) {

      this._productsService.getListado().subscribe(
          Response => {
              if (Response.result == true)
                  this.dataSource = new MatTableDataSource(Response.response.planes);

              if (Response.error != null)

                  Swal.fire({
                      title: 'Oops...',
                      text: 'No se pudieron obtener los datos del listado.'
                  })
          },
          error => {
              console.log(error);
          });
      this.getCarrito();
   

  }

  ngOnit() { }

  getCarrito() {
      this._productsService.getListadoCarrito().subscribe(
          Response => {
              if (Response.result == true)
              {

                this.dataSourceCarrito = new MatTableDataSource(Response.response);
                if (Response.response)
                this.cantidad = Response.response.length;
                else this.cantidad=0;
              }
              if (Response.result != true)

                  Swal.fire({
                      title: 'Oops...',
                      text: 'No se pudieron obtener los datos del listado.'
                  })
          },
          error => {
              console.log(error);
          });
  }

  agregarItem(ele: any, element: any) {
      this._productsService.agregarItem(ele, element.plan).subscribe(
          Response => {

            if(Response.result==true)
            {
                Swal.fire('Éxito', 'El producto ha sido añadido al carrito de forma exitosa', 'success');
                this.cantidad++;
                this.getCarrito();
            }
           
          },
          error => {
              console.log(error);
              Swal.fire('Error', 'Ups! Algo salio mal!', 'error');
          });
        
  }

  removerItem(element: any) {
      Swal.fire({
          title: '¿Estás seguro?',
          text: 'El producto será eliminado del carrito',
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonColor: '#d33',
          confirmButtonText: 'Confirmar',
          cancelButtonText: 'Cancelar',
      }).then((result) => {
          if (result.value) {
              this._productsService.removerItem(element).subscribe(
                  Response => {
                      Swal.fire('Éxito', 'El producto ha sido eliminado del carrito de forma exitosa', 'success');
                      for (let i = 0; i < this.dataSourceCarrito.data.length; i++) {
                          if (this.dataSourceCarrito.data[i].id_producto === element) {
                              this.dataSourceCarrito.data.splice(i, 1);
                              this.dataSourceCarrito._updateChangeSubscription();
                          }
                      }
                      this.cantidad--;

        },
                  error => {
                      console.log(error);
                      Swal.fire('Error', 'Ups! Algo salio mal!', 'error');
                  });
          }
      });
    //   this.getCarrito();

  }


}