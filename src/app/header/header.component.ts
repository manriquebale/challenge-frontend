import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Input() dataSource;
  @Input() dataSourceCarrito;
  @Input() listado;
  @Output() listadoChange = new EventEmitter < boolean > ();
  @Input() cantidad;

  constructor() {
  }

  ngOnInit() {}

  applyFilter(filterValue: string) {

      if (this.listado == true) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        this.dataSource.filterPredicate = function(data, filter: string): boolean {
        return data.nombre.toLowerCase().includes(filter)
      };
    }
      if (this.listado == false) {
          this.dataSourceCarrito.filter = filterValue.trim().toLowerCase();
          this.dataSourceCarrito.filterPredicate = function(data, filter: string): boolean {
          return data.nombre.toLowerCase().includes(filter)
      };
    }
  }

  cambiar(flag: boolean) {
          this.listado = flag;
          this.listadoChange.emit(this.listado);
  }

}