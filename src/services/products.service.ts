import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { APIURL } from './apiUrl';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  apiUrl: string;
  constructor(
      public _http: HttpClient
  ) {
      this.apiUrl = APIURL.url;
  }
  getListado(): Observable <any> {
      return this._http.get(APIURL.url + 'getListado.php');
      
  }
  
  agregarItem(ele: any, element: any): Observable <any> {
      const params = new HttpParams({
          fromObject: {
              plan: element,
              periodo: ele.periodo,
          }
      });
      let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded');
      return this._http.post(APIURL.url + 'agregarItem.php?' + params, {headers:headers});
  }

  getListadoCarrito(): Observable <any> {
      return this._http.get(APIURL.url + 'getListadoCarrito.php');
  }
  removerItem(element: any): Observable <any> {
      const params = new HttpParams({
          fromObject: {
              id_producto: element
          }
      });
      let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded');

      return this._http.post(APIURL.url + 'removerItem.php?'+ params,{headers: headers});
  }

}