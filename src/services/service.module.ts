// Modulos
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import {
   ProductsService
} from './services.index';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
  ],
  providers: [
   ProductsService,
  ],
  declarations: []
})
export class ServiceModule { }
